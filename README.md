# FoundryVTT DigitalOcean Config

My simple, personal, opinionated guide to deploying FoundryVTT on DigitalOcean. These instructions
provide:

* FoundryVTT, running with [Mark Feldhousen's docker
configuration](https://github.com/felddy/foundryvtt-docker).
* An nginx reverse-proxy in front of FoundryVTT to help serve static assets better.
* A free SSL certificate provided by Let's Encrypt, automatically renewed thanks to the EFF's
[certbot](https://certbot.eff.org/).
* A portable, docker-compose based deployment that automatically starts when your droplet boots.

## Requirements

* A DigitalOcean basic Droplet running Ubuntu 20.04
* A domain whose zone file [has been
configured](https://docs.digitalocean.com/products/networking/dns/how-to/add-domains/) to point to your Droplet
* An AWS bucket configured for public reads and an IAM user with write access to that bucket.
Foundry's [own guide](https://foundryvtt.com/article/aws-s3/) on setting up the S3 integration is
a great starting point for setting this up.

## Setup

1. First, you'll need to make sure you have docker and docker-compose installed. The docker-compose
   package in DigitalOcean's package mirror is a little dated, so we have to do some [extra
   work](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04)
   to get a more recent version.

   ```bash
   ssh root@mydroplethostname.com
   apt-get install docker.io
   curl -L "https://github.com/docker/compose/releases/download/1.29.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   chmod +x /usr/local/bin/docker-compose
   ```

2. Next, so that we're not doing everything as the root user, we need a dedicated user to manage all
   the relevant configuration and run the Foundry + nginx servers.

   ```bash
   useradd -m foundry
   chsh -s /bin/bash foundry
   usermod -aG docker foundry
   usermod -aG sudo foundry
   passwd foundry
   su foundry
   cd
   ```

3. Next, we add all the contents of this repository to the `foundry` user's home directory. You'll
   want to make a few different changes to the default values, including:

   * Adding your domains and email addresses to `init-letsencrypt.sh`.
   * Replacing all occurences of `example.com` with your primary domain name in
   `data/nginx.app.conf`.
   * Replacing all occurrences of `example.com` with your primary domain name in
   `docker-compose.yml`.
   * Adding your AWS secrets to `data/secrets/awsConfig.json`.
   * Adding your Foundry secrets to `data/secrets/secrets.json`.

4. We're in the home stretch! Currently, nginx won't start up correctly because it's missing
   the certificate files, but nginx is also responsible for creating those certificates (using
   certbot). We handle this by generating a dummy cert to start with:

   ```bash
   ./init-letsencrypt.sh
   ```

5. We're about ready to go. You could just run Foundry now by running `docker-compose up`, but we
   want our service to be a little more durable, so we're going to create a systemd service that
   starts our services when the server (re)boots. First, create the service configuration:

   ```ini
   # /etc/systemd/system/foundry.service

   [Unit]
   Description=Foundry Application Service
   Requires=docker.service
   After=docker.service

   [Service]
   Type=oneshot
   RemainAfterExit=yes
   WorkingDirectory=/home/foundry
   ExecStart=/usr/local/bin/docker-compose up -d
   ExecStop=/usr/local/bin/docker-compose down
   TimeoutStartSec=0
   User=foundry

   [Install]
   WantedBy=multi-user.target
   ```

   Once the file is in place, run the following commands to start your service:

   ```bash
   systemctl enable foundry
   systemctl start foundry

   ```


6. At this point, Foundry should be online and accessible at your configured domain. You'll need to
   accept the service agreement and log in using your Admin Access Key to begin configuring Foundry.


## Credits

* Some of the nginx-certbot integration code was copied from
[wmnnd/nginx-certbot](https://github.com/wmnnd/nginx-certbot) (see `LICENSE` for copyright info).
* For deploying Foundry itself, we use Mark Feldhousen's excellent
[Dockerfile](https://github.com/felddy/foundryvtt-docker).
